import java.io.*;

public class CpCommand {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java CpCommand <source_file> <destination_file>");
            System.exit(1);
        }

        String sourceFile = args[0];
        String destinationFile = args[1];

        try (InputStream in = new FileInputStream(sourceFile);
             OutputStream out = new FileOutputStream(destinationFile)) {
            byte[] buffer = new byte[1024];
            int length;

            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            System.out.println("File copied successfully.");
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
