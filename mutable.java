public class MutableImmutableDemo {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("hi");
        System.out.println("Original StringBuilder: " + sb);
        sb.append("hiiuiii");
        System.out.println("Modified StringBuilder: " + sb);
        String str1 = "47567";
        System.out.println("Original String: " + str1);
        String str2 = str1.concat("hello");
        System.out.println("Modified String: " + str2);
    }
}
