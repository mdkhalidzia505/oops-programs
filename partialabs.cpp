#include <iostream>
using namespace std;
class Vehicle {
public:
    void start() {
        cout << "Starting vehicle..." << endl;
    }
    virtual void stop() = 0;
};
class Car : public Vehicle {
public:
    void stop() {
        cout << "Stopping car..." << endl;
    }
};
class Motorcycle : public Vehicle {
public:
    void stop() {
        cout << "Stopping motorcycle..." << endl;
    }
};
int main() {
    Vehicle* vehicle1;
    Vehicle* vehicle2;
    Car car;
    Motorcycle motorcycle;
    vehicle1 = &car;
    vehicle2 = &motorcycle;
    vehicle1->start();
    vehicle1->stop();
    vehicle2->start();
    vehicle2->stop();
    return 0;
}
