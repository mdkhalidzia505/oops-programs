#include <iostream>
using namespace std;
class Shape {
public:
    virtual float area() = 0;
};
class Rectangle : public Shape {
private:
    float length;
    float width;
public:
    Rectangle(float l, float w) {
        length = l;
        width = w;
    }
    float area() {
        return length * width;
    }
};
class Circle : public Shape {
private:
    float radius;
public:
    Circle(float r) {
        radius = r;
    }
    float area() {
        return 3.14 * radius * radius;
    }
};
int main() {
    Shape* shape1;
    Shape* shape2;
    Rectangle rect(5, 10);
    Circle circle(7);
    shape1 = &rect;
    shape2 = &circle;
    cout << shape1->area() << endl;
    cout<< shape2->area() << endl;
    return 0;
}
