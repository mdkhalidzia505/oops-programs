#include <iostream>
using namespace std;
class Math {
public:
    int add(int a, int b) {
        return a + b;
    }
    double add(double a, double b) {
        return a + b;
    }
    int add(int a, int b, int c) {
        return a + b + c;
    }
};
int main() {
    Math math;
    int result1 = math.add(2, 3);
    cout << result1 << endl;
    double result2 = math.add(2.5, 3.5);
    cout<< result2 << endl;
    int result3 = math.add(2, 3, 4);
    cout << result3 << endl;
    return 0;
}
