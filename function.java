class Animal {
    public void makeSound() {
      System.out.println("Animal is making a sound");
    }
  }
  
  class Dog extends Animal {
    public void makeSound() {
      System.out.println("Dog is barking");
    }
  
    public void makeSound(int volume) {
      System.out.println("Dog is barking at volume " + volume);
    }
  }
  
  class Main {
    public static void main(String[] args) {
      Animal animal = new Animal();
      animal.makeSound(); 
  
      Dog dog = new Dog();
      dog.makeSound();
      dog.makeSound(5); 
  
      Animal dogAnimal = new Dog();
      dogAnimal.makeSound();
    }}
