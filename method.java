public class MethodOverloadingExample {

    public int add(int x, int y) {
        return x + y;
    }

    public double add(double x, double y) {
        return x + y;
    }

    public String add(String x, String y) {
        return x + " " + y;
    }

    public static void main(String[] args) {
        MethodOverloadingExample obj = new MethodOverloadingExample();
        int intSum = obj.add(10, 20);
        double doubleSum = obj.add(1.5, 2.5);
        String stringConcat = obj.add("Hello", "World");
        
        System.out.println("intSum: " + intSum);
        System.out.println("doubleSum: " + doubleSum);
        System.out.println("stringConcat: " + stringConcat);
    }
}
