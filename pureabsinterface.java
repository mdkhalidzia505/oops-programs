interface Shape {
    public void draw();
    public double area();
}

class Circle implements Shape {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void draw() {
        System.out.println("Drawing circle...");
    }

    public double area() {
        return Math.PI * radius * radius;
    }
}

class Rectangle implements Shape {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public void draw() {
        System.out.println("Drawing rectangle...");
    }

    public double area() {
        return width * height;
    }
}

public class AbstractionExample {
    public static void main(String[] args) {
        Shape shape1 = new Circle(5);
        Shape shape2 = new Rectangle(4, 6);

        shape1.draw();
        System.out.println("Area of circle: " + shape1.area());

        shape2.draw();
        System.out.println("Area of rectangle: " + shape2.area());
    }
}
