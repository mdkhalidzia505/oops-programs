import java.util.*;
import java.lang.*;

class Main {
  public static void main(String[] args) {
    int x;
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter A And B Values:");
    int num1= sc.nextInt();
    int num2= sc.nextInt();
    System.out.println(
        "Enter Which Arithmetic Operation\n1)Addition\n2)Subtraction\n3)Multiplication\n4)Division\n5)Remainder\n");
    int r = sc.nextInt();
    if (r == 1) {
      x=a+b;
      System.out.println("Addition: "+x);
    } else if (r == 2) {
      x=a-b;
      System.out.println("Subtraction: "+x);
    } else if (r == 3) {
      x=a*b;
      System.out.println("Multiplication: "+x);
    } else if (r == 4) {
      x=a/b;
      System.out.println("Division: "+x);
    } else if(r==5) {
      x=a%b;
      System.out.println("Remainder: "+x);
    }
    else
    {
      System.out.println("Select between 1-5 only.");
    }
  }
}
}

