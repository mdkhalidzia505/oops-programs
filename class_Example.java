import java.security.AccessController;
import java.security.Permission;
import java.security.Policy;


public class Example {


    public static void main(String[] args) {
        Policy policy = Policy.getPolicy();


        policy.refresh();


        Permission perm = new RuntimePermission("accessClassInPackage.sun.awt");
        boolean isGranted = AccessController.checkPermission(perm) == null;


        if (isGranted) {
            System.out.println("Permission is granted");
        } else {
            System.out.println("Permission is not granted");
        }
    }
}


