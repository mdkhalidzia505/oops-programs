#include<iostream>
using namespace std;
class Parent {
  private:
    int pvt = 1;




  protected:
    int prot = 2;




  public:
    int pub = 3;




    // function to access private member
    int getPvt() {
      return pvt;
    }
};




class PublicChild : public Parent {
  public:
    // function to access protected member from Parent
    int getProt() {
      return prot;
    }
};




class ProtectedChild : protected Parent {
  public:
    // function to access protected member from Parent
    int getProt() {
      return prot;
    }




    // function to access public member from Parent
    int getPub() {
      return pub;
    }
};




class PrivateChild : private Parent {
  public:
    // function to access protected member from Parent
    int getProt() {
      return prot;
    }




    // function to access public member from Parent
    int getPub() {
      return pub;
    }
};




int main() {
    PublicChild obj1;
  PrivateChild obj2;
  ProtectedChild obj3;
    cout<<"PUBLIC:"<<endl;
  cout << "Private = " << obj1.getPvt() << endl;
  cout << "Protected = " << obj1.getProt() << endl;
  cout << "Public = " << obj1.pub <<"\n"<< endl;
  cout<<"PROTECTED:"<<endl;
  cout << "Protected = " << obj2.getProt() << endl;
  cout << "Public = " << obj2.getPub() << endl;
  cout << "int parent::getpvt()' is inaccessible \n" << endl;
  cout << "PRIVATE" << endl;
  cout << "Protected = " << obj3.getProt() << endl;
  cout << "Public = " << obj3.getPub() << endl;
  cout << "'parent' is not an accessible parent of 'childPvt'" << endl;
}
