#include<iostream>
using namespace std;
class Parent{
    public:
    void father() {
        cout<<"the father method"<<endl;
    }
    void mother() {
        cout<<"the mother method"<<endl;
    }
};
class Child : public Parent{
    public:
    void kid() {
        cout<<"the child" <<endl;
    }
};
int main() {
    child obj;
    obj.father();
    obj.mother();
    obj.kid();
    return 0;
}
