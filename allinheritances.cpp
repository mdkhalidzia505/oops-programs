#include <iostream>
using namespace std;
class Grandpa{
  public:
  Grandpa(){
    cout<<"MULTI LEVEL INHERITANCE: "<<endl;
    cout<<"grandparent class\n";
  }
};
class Parent1{
   public:
   Parent1()
   {
   cout<<"SINGLE INHERITANCE: "<<endl;
   cout<<"parent1 class\n";  
   }
};
class Parent2{
   public:
   Parent2(){
   cout<<"parent2 class\n";
   }
};
class Parent3 : public Grandpa{
  public:
  Parent3(){
  cout<<"parent3 class\n";
  }
};
class Parent4{
  public:
  Parent4(){
      cout<<"MULTIPLE INHERITANCE "<<endl;
      cout<<"parent4 class \n";
  }
};
class Child1 : public Parent1{
    public:
    Child1(){
        cout<<"child1 derived from parent1 \n"<<endl;  
    }
};
class Child2 : public Parent4 , public Parent2{
    public:
    Child2(){
     cout<<"child2 class derived from parent4 and parent 2\n"<<endl;
    }
};
class Child3 : public Parent3{
  public:
  Child3(){
     cout<<"child3 class derived from parent3 \n"<<endl;
  }
};
int main()
{
    Child1 obj1;
    Child2 obj2;
    Child3 obj3;
}
