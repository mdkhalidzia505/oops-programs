#include<iostream>
using namespace std;
class Complex{
    private:
    float real;
    float imag;
    public:
    Complex(){
        real=0;
        imag=0;
    }
 void input(){
    cout<<"Enter value"<<endl;
    cin>>real;
    cin>>imag;
 }
 Complex operator +(const Complex& obj){
    Complex temp;
    temp.real=real+obj.real;
    temp.imag=imag+obj.imag;
    return temp;
 } 
 void output(){
    if(imag<0){
        cout<<real<<imag<<"i";
    }
    else
    cout<<real<<"+"<<imag<<"i";
 }
};
int main(){
    Complex complex1,complex2,result;
    cout<<"Enter first complex value"<<endl;
    complex1.input();
    cout<<"Enter second complex value"<<endl;
    complex2.input();
    result=complex1+complex2;
    result.output();
    return 0;
}
